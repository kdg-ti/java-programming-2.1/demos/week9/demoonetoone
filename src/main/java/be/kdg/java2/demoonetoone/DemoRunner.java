package be.kdg.java2.demoonetoone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Component
public class DemoRunner implements CommandLineRunner {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {
        EntityManager em = entityManagerFactory.createEntityManager();
        Author author = new Author("Herman", "Brusselmans");
        AuthorDetail authorDetail = new AuthorDetail("h@brussel.com");
        author.setAuthorDetail(authorDetail);
        em.getTransaction().begin();
        em.persist(author);
        em.getTransaction().commit();
        em.close();

        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        authorDetail = em.find(AuthorDetail.class, 1);
        System.out.println("Lazy or Eager?");
        System.out.println("Author: " + authorDetail.getAuthor());
        em.getTransaction().commit();
        em.close();

    }
}
